Package.describe({
  name: 'sparkline',
  version: '0.1.1',
  summary: 'Blaze component for jQuery Sparklines',
  git: 'https://bitbucket.org/aspirinchaos/sparkline.git',
  documentation: 'README.md',
});

Npm.depends({
  'jquery-sparkline': '2.4.0',
});

Package.onUse((api) => {
  api.versionsFrom('1.5');
  api.use([
    'ecmascript',
    'tmeasday:check-npm-versions',
    'templating',
    'jquery',
    'template-controller',
  ]);
  api.mainModule('sparkline.js', 'client');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('sparkline');
  api.mainModule('sparkline-tests.js');
});
