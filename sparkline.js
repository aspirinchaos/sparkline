import { TemplateController } from 'meteor/template-controller';
import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';
import SimpleSchema from 'simpl-schema';
import 'jquery-sparkline';
import './sparkline.html';

checkNpmVersions({ 'simpl-schema': '1.5.3' }, 'sparkline');

TemplateController('Sparkline', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    // тип графика
    type: {
      type: String,
      allowedValues: ['line', 'bar', 'tristate', 'discrete', 'bullet', 'pie', 'box'],
    },
    values: { type: Array },
    'values.$': { type: Number },
    // опции для графика, проверяется самим sparkline
    options: { type: Object, blackbox: true, defaultValue: {} },
    // функция для возврата объекта графика
    handleObject: { type: Function, optional: true },
    className: { type: String, defaultValue: '' },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
    this.autorun(() => {
      const {
        type, values, options,
      } = this.props;
      this.spark = this.$('span').sparkline(values, {
        type,
        ...options,
      });
    });
    // Если нужно вернуть график для работы с ним
    if (this.props.handleObject) {
      this.props.handleObject(this.spark);
    }
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {},

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    spark: null,
  },
});
