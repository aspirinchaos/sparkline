# Chartist

Blaze-Компонент реализующий работу графиков [sparkline](https://omnipotent.net/jquery.sparkline).
Sparklines - это небольшие строчные графики, формируются из массива чисел.

### Вызов компонета
```spacebars
{{> Sparkline spark}}
```
### Параметры
```javascript
const props = {
  // Тип графика
  type: {
    type: String,
    allowedValues: ['line', 'bar', 'tristate', 'discrete', 'bullet', 'pie', 'box'],
  },
  // Массив значений
  values: { type: Array },
  // Если будут строки, то они преобразуются в число автоматически
  'values.$': { type: Number },
  // опции для графика, проверяется самим sparkline
  options: { type: Object, blackbox: true, defaultValue: {} },
  // функция для возврата объекта графика
  handleObject: { type: Function, optional: true },
  // класс для обертки графика
  className: { type: String, defaultValue: '' },
}
``` 

