// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by sparkline.js.
import { name as packageName } from "meteor/sparkline";

// Write your tests here!
// Here is an example.
Tinytest.add('sparkline - example', function (test) {
  test.equal(packageName, "sparkline");
});
